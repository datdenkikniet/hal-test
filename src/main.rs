#![no_main]
#![no_std]

use cortex_m::peripheral::DWT;
use panic_rtt_target as _;
use rtic::app;
use rtt_target::{rprintln, rtt_init_print};
use stm32l4xx_hal::{
    adc::{SampleTime, Sequence, ADC},
    delay::DelayCM,
    prelude::*,
    rcc::{ClockSecuritySystem, CrystalBypass, MsiFreq},
    time::Hertz,
};

use aes_ccm::aead::heapless::Vec;

use chacha20poly1305::aead::{AeadInPlace, NewAead};
use chacha20poly1305::{
    aead::generic_array::{typenum::U128, GenericArray},
    ChaCha8Poly1305,
}; // Or `XChaCha20Poly1305`

#[app(device = stm32l4xx_hal::stm32, peripherals = true, monotonic = rtic::cyccnt::CYCCNT)]
const APP: () = {
    // RTIC app is written in here!

    #[init]
    fn init(cx: init::Context) {
        rtt_init_print!();

        // rprintln!("Hello from init!");

        let mut cp = cx.core;

        cp.DCB.enable_trace();
        cp.DWT.enable_cycle_counter();

        let pac = cx.device;

        let mut rcc = pac.RCC.constrain();
        let mut flash = pac.FLASH.constrain();
        let mut pwr = pac.PWR.constrain(&mut rcc.apb1r1);
        // let mut gpioa = pac.GPIOA.split(&mut rcc.ahb2);

        //
        // Initialize the clocks
        //
        let clocks = rcc
            .cfgr
            .lse(CrystalBypass::Enable, ClockSecuritySystem::Disable)
            .hsi48(true)
            .msi(MsiFreq::RANGE4M)
            .sysclk(Hertz(80_000_000))
            .freeze(&mut flash.acr, &mut pwr);

        let key = [
            0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5,
            0xC6, 0xC7, 0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC0, 0xC1, 0xC2, 0xC3,
            0xC4, 0xC5, 0xC6, 0xC7,
        ];

        let nonce = [
            0x00, 0x00, 0x00, 0x03, 0x02, 0x01, 0x00, 0xA0, 0xA0, 0xA0, 0xA0, 0xA0,
        ];
        let plaintext = [
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
            0x16, 0x17,
        ];
        let associated_data = [0x00, 0x01, 0x02, 0x03, 0x04];

        let chacha = ChaCha8Poly1305::new(&GenericArray::clone_from_slice(&key));

        let mut buffer: Vec<u8, U128> = Vec::new();
        buffer.extend_from_slice(&plaintext).unwrap();

        rprintln!("{:?}", DWT::get_cycle_count());

        if let Ok(_) = chacha.encrypt_in_place(
            &GenericArray::clone_from_slice(&nonce),
            &associated_data,
            &mut buffer,
        ) {}

        rprintln!("{:?}", DWT::get_cycle_count());

        // rprintln!("{:?}", buffer);

        let mut delay = DelayCM::new(clocks);

        let mut adc = ADC::new(
            pac.ADC1,
            pac.ADC_COMMON,
            &mut rcc.ahb2,
            &mut rcc.ccipr,
            &mut delay,
        );

        let mut temp_pin = adc.enable_temperature();
        // rprintln!("{:#0x}", adc.read(&mut temp_pin).unwrap());
        // let mut vref_pin = adc.enable_vref(&mut delay);

        // adc.configure_sequence(&mut vref_pin, Sequence::Two, SampleTime::Cycles640_5);
        // // adc.configure_sequence(&mut temp_pin, Sequence::Three, SampleTime::Cycles640_5);
        // // adc.configure_sequence(&mut temp_pin, Sequence::Four, SampleTime::Cycles640_5);
        // // adc.configure_sequence(&mut temp_pin, Sequence::Five, SampleTime::Cycles640_5);

        // let mut data = [0; 10];
        // let mut index = 0;
        adc.enable();

        adc.configure_sequence(&mut temp_pin, Sequence::One, SampleTime::Cycles640_5);

        adc.start_conversion();

        while !adc.has_completed_sequence() {}
        // rprintln!("{:#0x}", adc.get_adc_data());

        adc.start_conversion();
        while !adc.has_completed_sequence() {}
        // rprintln!("{:#0x}", adc.get_adc_data());

        // adc.start_conversion();
        // let mut busy_wait = 0;

        // loop {
        //     let conversion_complete = adc.has_completed_conversion();
        //     if conversion_complete {
        //         data[index] = adc.get_adc_data();
        //         index += 1;
        //         continue;
        //     }

        //     let sequence_complete = adc.has_completed_sequence();
        //     if sequence_complete {
        //         rprintln!("Sequence completed! {}, {:?}. {}", index, data, busy_wait);
        //         break;
        //     }
        //     busy_wait += 1;
        // }
    }

    #[idle]
    fn idle(_cx: idle::Context) -> ! {
        loop {}
    }

    extern "C" {
        fn DFSDM1();
    }
};
