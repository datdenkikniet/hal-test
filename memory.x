# Don't name this file memory.x
# If it's named that, the linker will always use it instead of whatever
# memory.x we've copied into the build folder. (see build.rs)

MEMORY
{
    FLASH  :     ORIGIN = 0x08000000, LENGTH = 104K
    BOOTLOADER : ORIGIN = 0x08000000, LENGTH = 20k
    FW_HEADER :  ORIGIN = 0x08005000, LENGTH = 2k
    # FLASH :      ORIGIN = 0x08005800, LENGTH = 104k
    STORAGE  :   ORIGIN = 0x0801F800, LENGTH = 2k
    RAM :        ORIGIN = 0x20000000, LENGTH = 39K
    PANDUMP :    ORIGIN = 0x20009C00, LENGTH = 1K
}

PROVIDE(__flash_memory_start__ = ORIGIN(BOOTLOADER));
PROVIDE(__flash_memory_end__ = ORIGIN(STORAGE) + LENGTH(STORAGE));
PROVIDE(__flash_start__ = ORIGIN(FLASH));
PROVIDE(__flash_end__ = ORIGIN(FLASH) + LENGTH(FLASH));
PROVIDE(__storage_start__ = ORIGIN(STORAGE));
PROVIDE(__storage_end__ = ORIGIN(STORAGE) + LENGTH(STORAGE));
PROVIDE(_panic_dump_start = ORIGIN(PANDUMP));
PROVIDE(_panic_dump_end = ORIGIN(PANDUMP) + LENGTH(PANDUMP));
